﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T : BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync(params Expression<Func<T, object>>[] includeProperties);

        Task<IEnumerable<T>> GetByFilterAsync(Expression<Func<T, bool>> filter, params Expression<Func<T, object>>[] includeProperties);

        Task<T> GetByIdAsync(Guid id, params Expression<Func<T, object>>[] includeProperties);

        Task<bool> AnyAsync(Guid id);
        Task<bool> AnyAsync(Expression<Func<T, bool>> filter);

        Task AddAsync(T entity);
        Task AddRangeAsync(IEnumerable<T> entities);

        Task UpdateAsync(T entity);

        Task RemoveAsync(Guid id);
        Task RemoveAsync(Expression<Func<T, bool>> filter);
    }
}