﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    [Table("employee")]
    [Description("Сотрудники")]
    [Index("Email", IsUnique = true, Name = "EmailIndexEmpl")]
    public class Employee: BaseEntity
    {
        [MaxLength(200), Required(AllowEmptyStrings = false)]
        public string FirstName { get; set; }
        [MaxLength(200)]
        public string LastName { get; set; }

        [IgnoreDataMember]
        public string FullName => $"{FirstName} {LastName}";

        [MaxLength(300)]
        public string Email { get; set; }
        public int AppliedPromocodesCount { get; set; }

        [ForeignKey("Role")]
        public Guid RoleId { get; set; }
        [ForeignKey("RoleId")]
        public virtual Role Role { get; set; }

    }
}