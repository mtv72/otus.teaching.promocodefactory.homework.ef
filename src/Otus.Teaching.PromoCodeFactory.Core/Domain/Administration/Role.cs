﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    [Table("role")]
    [Description("Роли")]
    [Index("Name", IsUnique = true, Name = "RoleName")]
    public class Role: BaseEntity
    {
        [MaxLength(200), Required(AllowEmptyStrings = false)]
        public string Name { get; set; }

        [MaxLength(500)]
        public string Description { get; set; }
        public virtual ICollection<Employee> Employees { get; set; }
    }
}