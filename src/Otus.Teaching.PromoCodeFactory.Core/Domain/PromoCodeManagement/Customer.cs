﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    [Table("customer")]
    [Description("Клиенты")]
    [Index("Email", IsUnique = true, Name = "EmailIndex")]
    public class Customer:BaseEntity
    {
        [MaxLength(200), Required(AllowEmptyStrings = false)]
        public string FirstName { get; set; }
        [MaxLength(200)]
        public string LastName { get; set; }

        [IgnoreDataMember]
        public string FullName => $"{FirstName} {LastName}";

        [MaxLength(300)]
        public string Email { get; set; }

        public virtual ICollection<CustomerPreference> CustomerPreferences { get; set; }
        public virtual ICollection<PromoCode> PromoCodes { get; set; } 
        //TODO: Списки Preferences и Promocodes 
    }
}