﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    [Table("customer_preference")]
    [Description("Предпочтения клиента")]
    [Index("PreferenceId", "CustomerId", IsUnique = true, Name = "CustomerPreference_PreferenceId_CustomerId")]
    public class CustomerPreference : BaseEntity
    {
        [ForeignKey("Preference")]
        public Guid PreferenceId { get; set; }

        [ForeignKey("Customer")]
        public Guid CustomerId { get; set; }

        [ForeignKey("CustomerId")]
        public virtual Customer Customer { get; set; }
       
        [ForeignKey("PreferenceId")]
        public virtual Preference Preference { get; set; }
    }
}
