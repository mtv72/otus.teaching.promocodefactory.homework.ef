﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    [Table("preference")]
    [Description("Предпочтения")]
    [Index("Name", IsUnique = true, Name = "PreferenceName")]
    public class Preference:BaseEntity
    {
        [MaxLength(150), Required(AllowEmptyStrings = false)]
        public string Name { get; set; }
        public virtual ICollection<CustomerPreference> CustomerPreferences { get; set; }
        public virtual ICollection<PromoCode> PromoCodes { get; set; }
    }
}