﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    [Table("promo_code")]
    [Description("Промо-коды")]
    [Index("Code", IsUnique = true, Name = "PromoCodeCode")]
    public class PromoCode: BaseEntity
    {
        [MaxLength(200), Required(AllowEmptyStrings = false)]
        public string Code { get; set; }

        [MaxLength(200)]
        public string ServiceInfo { get; set; }

        public DateTimeOffset BeginDate { get; set; }

        public DateTimeOffset EndDate { get; set; }

        [MaxLength(300)]
        public string PartnerName { get; set; }

        [ForeignKey("Preference")]
        public Guid PreferenceId { get; set; }
     
        [ForeignKey("PreferenceId")]        
        public virtual Preference Preference { get; set; }

        [ForeignKey("Customer")]
        public Guid? CustomerId { get; set; }
      
        [ForeignKey("CustomerId")]
        public virtual Customer Customer { get; set; }

        [ForeignKey("PartnerManager")]
        public Guid PartnerManagerId { get; set; }
     
        [ForeignKey("PartnerManagerId")]
        public virtual Employee PartnerManager { get; set; }
    }
}