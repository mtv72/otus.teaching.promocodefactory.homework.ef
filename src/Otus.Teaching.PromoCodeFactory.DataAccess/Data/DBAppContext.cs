﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class DBAppContext : DbContext
    {

        public DBAppContext() { }

        public DbSet<Employee> Employees { get; set; } = null!;
        public DbSet<Role> Roles { get; set; } = null!;
        public DbSet<Customer> Customers { get; set; } = null!;
        public DbSet<Preference> Preferences { get; set; } = null!;
        public DbSet<PromoCode> PromoCodes { get; set; } = null!;

        public DBAppContext(DbContextOptions options) : base(options)
        {
            //Database.EnsureDeleted();
            //Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //base.OnModelCreating(modelBuilder);

            //modelBuilder.Entity<Role>()
            //       .HasIndex(i => i.Name)
            //       .IsUnique();

            //modelBuilder.Entity<Employee>()
            //  .HasOne(u => u.Role)
            //    .WithMany(c => c.Employees)
            //    .HasForeignKey(u => u.RoleId);

            //modelBuilder.Entity<Customer>();

            //modelBuilder.Entity<Preference>(entity =>
            //{
            //    entity.HasIndex(i => i.Name).IsUnique();

            //    entity.HasMany(u => u.PromoCodes)
            //    .WithOne(c => c.Preference)
            //    .HasForeignKey(c => c.PreferenceId);
            //});

            //modelBuilder.Entity<PromoCode>(entity =>
            //{
            //    entity.HasIndex(i => i.Code).IsUnique();

            //    entity.HasOne(u => u.Customer)
            //    .WithMany(c => c.PromoCodes)
            //    .HasForeignKey(u => u.CustomerId);

            //});

            //modelBuilder.Entity<CustomerPreference>(entity =>
            //{
            //    entity.HasIndex(i => new { i.CustomerId, i.PreferenceId },
            //        "PromoCodeCode")
            //    .IsUnique();

            //    entity.HasOne(u => u.Customer)
            //    .WithMany(c => c.CustomerPreferences)
            //    .HasForeignKey(u => u.CustomerId);

            //    entity.HasOne(u => u.Preference)
            //    .WithMany(c => c.CustomerPreferences)
            //    .HasForeignKey(u => u.PreferenceId);
            //});



            modelBuilder.Entity<Role>().HasData(FakeDataFactory.Roles);
            modelBuilder.Entity<Employee>().HasData(FakeDataFactory.Employees);
            modelBuilder.Entity<Preference>().HasData(FakeDataFactory.Preferences);
            modelBuilder.Entity<Customer>().HasData(FakeDataFactory.Customers);
            modelBuilder.Entity<CustomerPreference>().HasData(FakeDataFactory.CustomersPreferences);
        }
    }
}
