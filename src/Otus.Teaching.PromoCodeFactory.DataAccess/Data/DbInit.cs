﻿using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class DbInit 
    {
        protected DBAppContext _db;
        protected ILogger _logger;
        public DbInit(ILogger<DbInit> logger, DBAppContext context) 
        {
            _logger = logger;
            _db = context;
        }
    }
}
