﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Migrations
{
    public partial class Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "customer",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "TEXT", nullable: false),
                    FirstName = table.Column<string>(type: "TEXT", maxLength: 200, nullable: false),
                    LastName = table.Column<string>(type: "TEXT", maxLength: 200, nullable: true),
                    Email = table.Column<string>(type: "TEXT", maxLength: 300, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_customer", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "preference",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "TEXT", nullable: false),
                    Name = table.Column<string>(type: "TEXT", maxLength: 150, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_preference", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "role",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "TEXT", nullable: false),
                    Name = table.Column<string>(type: "TEXT", maxLength: 200, nullable: false),
                    Description = table.Column<string>(type: "TEXT", maxLength: 500, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_role", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "customer_preference",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "TEXT", nullable: false),
                    PreferenceId = table.Column<Guid>(type: "TEXT", nullable: false),
                    CustomerId = table.Column<Guid>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_customer_preference", x => x.Id);
                    table.ForeignKey(
                        name: "FK_customer_preference_customer_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "customer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_customer_preference_preference_PreferenceId",
                        column: x => x.PreferenceId,
                        principalTable: "preference",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "employee",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "TEXT", nullable: false),
                    FirstName = table.Column<string>(type: "TEXT", maxLength: 200, nullable: false),
                    LastName = table.Column<string>(type: "TEXT", maxLength: 200, nullable: true),
                    Email = table.Column<string>(type: "TEXT", maxLength: 300, nullable: true),
                    AppliedPromocodesCount = table.Column<int>(type: "INTEGER", nullable: false),
                    RoleId = table.Column<Guid>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_employee", x => x.Id);
                    table.ForeignKey(
                        name: "FK_employee_role_RoleId",
                        column: x => x.RoleId,
                        principalTable: "role",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "promo_code",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "TEXT", nullable: false),
                    Code = table.Column<string>(type: "TEXT", maxLength: 200, nullable: false),
                    ServiceInfo = table.Column<string>(type: "TEXT", maxLength: 200, nullable: true),
                    BeginDate = table.Column<DateTime>(type: "TEXT", nullable: false),
                    EndDate = table.Column<DateTime>(type: "TEXT", nullable: false),
                    PartnerName = table.Column<string>(type: "TEXT", maxLength: 300, nullable: true),
                    PreferenceId = table.Column<Guid>(type: "TEXT", nullable: false),
                    CustomerId = table.Column<Guid>(type: "TEXT", nullable: true),
                    PartnerManagerId = table.Column<Guid>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_promo_code", x => x.Id);
                    table.ForeignKey(
                        name: "FK_promo_code_customer_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "customer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_promo_code_employee_PartnerManagerId",
                        column: x => x.PartnerManagerId,
                        principalTable: "employee",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_promo_code_preference_PreferenceId",
                        column: x => x.PreferenceId,
                        principalTable: "preference",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "customer",
                columns: new[] { "Id", "Email", "FirstName", "LastName" },
                values: new object[] { new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), "ivan_sergeev@mail.ru", "Иван", "Петров" });

            migrationBuilder.InsertData(
                table: "preference",
                columns: new[] { "Id", "Name" },
                values: new object[] { new Guid("ef7f299f-92d7-459f-896e-078ed53ef99c"), "Театр" });

            migrationBuilder.InsertData(
                table: "preference",
                columns: new[] { "Id", "Name" },
                values: new object[] { new Guid("c4bda62e-fc74-4256-a956-4760b3858cbd"), "Семья" });

            migrationBuilder.InsertData(
                table: "preference",
                columns: new[] { "Id", "Name" },
                values: new object[] { new Guid("76324c47-68d2-472d-abb8-33cfa8cc0c84"), "Дети" });

            migrationBuilder.InsertData(
                table: "preference",
                columns: new[] { "Id", "Name" },
                values: new object[] { new Guid("ad2bc0fe-4ee2-4dc8-9799-ca8189f9a400"), "Спорт" });

            migrationBuilder.InsertData(
                table: "preference",
                columns: new[] { "Id", "Name" },
                values: new object[] { new Guid("857992f8-a3df-420f-8b45-4b33699f89b2"), "Путешествия" });

            migrationBuilder.InsertData(
                table: "role",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[] { new Guid("53729686-a368-4eeb-8bfa-cc69b6050d02"), "Администратор", "Admin" });

            migrationBuilder.InsertData(
                table: "role",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[] { new Guid("b0ae7aac-5493-45cd-ad16-87426a5e7665"), "Партнерский менеджер", "PartnerManager" });

            migrationBuilder.InsertData(
                table: "customer_preference",
                columns: new[] { "Id", "CustomerId", "PreferenceId" },
                values: new object[] { new Guid("6aa7f3b1-b589-44d5-876b-0dd052ffd1f2"), new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), new Guid("c4bda62e-fc74-4256-a956-4760b3858cbd") });

            migrationBuilder.InsertData(
                table: "customer_preference",
                columns: new[] { "Id", "CustomerId", "PreferenceId" },
                values: new object[] { new Guid("c19079a3-a46c-4948-80f5-e92583cc70ad"), new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), new Guid("ad2bc0fe-4ee2-4dc8-9799-ca8189f9a400") });

            migrationBuilder.InsertData(
                table: "customer_preference",
                columns: new[] { "Id", "CustomerId", "PreferenceId" },
                values: new object[] { new Guid("cda01f41-fd01-43c1-9543-bd35b2c3050f"), new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), new Guid("857992f8-a3df-420f-8b45-4b33699f89b2") });

            migrationBuilder.InsertData(
                table: "employee",
                columns: new[] { "Id", "AppliedPromocodesCount", "Email", "FirstName", "LastName", "RoleId" },
                values: new object[] { new Guid("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"), 5, "owner@somemail.ru", "Иван", "Сергеев", new Guid("53729686-a368-4eeb-8bfa-cc69b6050d02") });

            migrationBuilder.InsertData(
                table: "employee",
                columns: new[] { "Id", "AppliedPromocodesCount", "Email", "FirstName", "LastName", "RoleId" },
                values: new object[] { new Guid("f766e2bf-340a-46ea-bff3-f1700b435895"), 10, "andreev@somemail.ru", "Петр", "Андреев", new Guid("b0ae7aac-5493-45cd-ad16-87426a5e7665") });

            migrationBuilder.CreateIndex(
                name: "CustomerPreference_PreferenceId_CustomerId",
                table: "customer_preference",
                columns: new[] { "PreferenceId", "CustomerId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_customer_preference_CustomerId",
                table: "customer_preference",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_employee_RoleId",
                table: "employee",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "PreferenceName",
                table: "preference",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_promo_code_CustomerId",
                table: "promo_code",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_promo_code_PartnerManagerId",
                table: "promo_code",
                column: "PartnerManagerId");

            migrationBuilder.CreateIndex(
                name: "IX_promo_code_PreferenceId",
                table: "promo_code",
                column: "PreferenceId");

            migrationBuilder.CreateIndex(
                name: "PromoCodeCode",
                table: "promo_code",
                column: "Code",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "RoleName",
                table: "role",
                column: "Name",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "customer_preference");

            migrationBuilder.DropTable(
                name: "promo_code");

            migrationBuilder.DropTable(
                name: "customer");

            migrationBuilder.DropTable(
                name: "employee");

            migrationBuilder.DropTable(
                name: "preference");

            migrationBuilder.DropTable(
                name: "role");
        }
    }
}
