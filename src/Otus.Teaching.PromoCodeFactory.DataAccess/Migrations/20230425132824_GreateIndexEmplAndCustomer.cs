﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Migrations
{
    public partial class GreateIndexEmplAndCustomer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "customer_preference",
                keyColumn: "Id",
                keyValue: new Guid("6aa7f3b1-b589-44d5-876b-0dd052ffd1f2"));

            migrationBuilder.DeleteData(
                table: "customer_preference",
                keyColumn: "Id",
                keyValue: new Guid("c19079a3-a46c-4948-80f5-e92583cc70ad"));

            migrationBuilder.DeleteData(
                table: "customer_preference",
                keyColumn: "Id",
                keyValue: new Guid("cda01f41-fd01-43c1-9543-bd35b2c3050f"));

            migrationBuilder.InsertData(
                table: "customer_preference",
                columns: new[] { "Id", "CustomerId", "PreferenceId" },
                values: new object[] { new Guid("c1165eaf-3fce-485e-a8ae-118c43786a8f"), new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), new Guid("ad2bc0fe-4ee2-4dc8-9799-ca8189f9a400") });

            migrationBuilder.InsertData(
                table: "customer_preference",
                columns: new[] { "Id", "CustomerId", "PreferenceId" },
                values: new object[] { new Guid("71c6ec42-3b34-4b4f-b99d-cc366d8dff23"), new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), new Guid("857992f8-a3df-420f-8b45-4b33699f89b2") });

            migrationBuilder.InsertData(
                table: "customer_preference",
                columns: new[] { "Id", "CustomerId", "PreferenceId" },
                values: new object[] { new Guid("0901d73d-c65c-4ad7-b315-c85ddd9bb458"), new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), new Guid("c4bda62e-fc74-4256-a956-4760b3858cbd") });

            migrationBuilder.CreateIndex(
                name: "EmailIndexEmpl",
                table: "employee",
                column: "Email",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "customer",
                column: "Email",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "EmailIndexEmpl",
                table: "employee");

            migrationBuilder.DropIndex(
                name: "EmailIndex",
                table: "customer");

            migrationBuilder.DeleteData(
                table: "customer_preference",
                keyColumn: "Id",
                keyValue: new Guid("0901d73d-c65c-4ad7-b315-c85ddd9bb458"));

            migrationBuilder.DeleteData(
                table: "customer_preference",
                keyColumn: "Id",
                keyValue: new Guid("71c6ec42-3b34-4b4f-b99d-cc366d8dff23"));

            migrationBuilder.DeleteData(
                table: "customer_preference",
                keyColumn: "Id",
                keyValue: new Guid("c1165eaf-3fce-485e-a8ae-118c43786a8f"));

            migrationBuilder.InsertData(
                table: "customer_preference",
                columns: new[] { "Id", "CustomerId", "PreferenceId" },
                values: new object[] { new Guid("c19079a3-a46c-4948-80f5-e92583cc70ad"), new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), new Guid("ad2bc0fe-4ee2-4dc8-9799-ca8189f9a400") });

            migrationBuilder.InsertData(
                table: "customer_preference",
                columns: new[] { "Id", "CustomerId", "PreferenceId" },
                values: new object[] { new Guid("cda01f41-fd01-43c1-9543-bd35b2c3050f"), new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), new Guid("857992f8-a3df-420f-8b45-4b33699f89b2") });

            migrationBuilder.InsertData(
                table: "customer_preference",
                columns: new[] { "Id", "CustomerId", "PreferenceId" },
                values: new object[] { new Guid("6aa7f3b1-b589-44d5-876b-0dd052ffd1f2"), new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), new Guid("c4bda62e-fc74-4256-a956-4760b3858cbd") });
        }
    }
}
