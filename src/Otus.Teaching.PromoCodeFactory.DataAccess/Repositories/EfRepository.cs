﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly DBAppContext _db;
        private readonly DbSet<T> _dbSet;
        public EfRepository(DBAppContext databaseContext)
        {
            _db = databaseContext;
            _dbSet = _db.Set<T>();
        }
        private static IQueryable<T> Include(IQueryable<T> query, params Expression<Func<T, object>>[] includeProperties)
        {
            if (includeProperties == null) return query;

            return includeProperties.Aggregate(query, (current, includeProperty) => current.Include(includeProperty));
        }

        private static IQueryable<T> GetQueryable(DbSet<T> dbSet, params Expression<Func<T, object>>[] includeProperties)
            => Include(dbSet.AsNoTracking(), includeProperties);

        public async Task<IEnumerable<T>> GetAllAsync(params Expression<Func<T, object>>[] includeProperties)
            => await GetQueryable(_dbSet, includeProperties).ToListAsync();

        public async Task<IEnumerable<T>> GetByFilterAsync(Expression<Func<T, bool>> filter,params Expression<Func<T, object>>[] includeProperties)
            => await GetQueryable(_dbSet, includeProperties).Where(filter).ToListAsync();

        public Task<T> GetByIdAsync(Guid id,params Expression<Func<T, object>>[] includeProperties)
            => GetQueryable(_dbSet, includeProperties).FirstOrDefaultAsync(_ => _.Id == id);

        public Task<bool> AnyAsync(Guid id)
            => _dbSet.AsNoTracking().AnyAsync(_ => _.Id == id);

        public Task<bool> AnyAsync(Expression<Func<T, bool>> filter)
            => _dbSet.AsNoTracking().AnyAsync(filter);

        public async Task AddAsync(T entity)
        {
            if (await AnyAsync(entity.Id))
            {
                ThrowConflictException();
            }

            await _dbSet.AddAsync(entity);
            await _db.SaveChangesAsync();
        }

        public async Task AddRangeAsync(IEnumerable<T> entities)
        {
            var ids = entities.Select(e => e.Id).ToList();

            if (await _dbSet.AnyAsync(x => ids.Contains(x.Id)))
            {
                ThrowConflictException();
            }

            await _dbSet.AddRangeAsync(entities);
            await _db.SaveChangesAsync();
        }

        public async Task RemoveAsync(Guid id)
        {
            var entityItem = await _dbSet.FirstOrDefaultAsync(_ => _.Id == id);

            if (!await AnyAsync(id))
            {
                ThrowNotFountException();
            }

            _dbSet.Remove(entityItem);

            await _db.SaveChangesAsync();
        }

        public async Task RemoveAsync(Expression<Func<T, bool>> filter)
        {
            if (!await AnyAsync(filter))
            {
                return;
            }

            var entityItems = await _dbSet.Where(filter).ToListAsync();

            _dbSet.RemoveRange(entityItems);

            await _db.SaveChangesAsync();
        }

        public async Task UpdateAsync(T entity)
        {
            if (!await AnyAsync(entity.Id))
            {
                ThrowNotFountException();
            }

            _dbSet.Update(entity);

            await _db.SaveChangesAsync();
        }
        private void ThrowNotFountException() => throw new ValidationException("С данным ключом значение не найдено!");

        private void ThrowConflictException() => throw new ValidationException("С данным ключом значение добавлено ранее!");
    }
}
