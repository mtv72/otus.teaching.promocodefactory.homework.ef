﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Microsoft.Extensions.Logging;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    public class BaseAPIController :  ControllerBase
    {
        protected DBAppContext _db;
        protected ILogger _logger;
        public BaseAPIController( ILogger logger, DBAppContext context) : base()
        {
            _logger = logger;
            _db = context;
        }
    }
}
