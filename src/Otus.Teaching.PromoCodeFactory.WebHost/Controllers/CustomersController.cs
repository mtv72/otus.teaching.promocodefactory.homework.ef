﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Http;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController : BaseAPIController
    {
        private readonly IUnitOfWork _unitOfWork;
        public CustomersController(IUnitOfWork uow, ILogger<RolesController> logger, DBAppContext context) : base(logger, context)
        {
            _unitOfWork = uow;
        }
        /// <summary>
        /// Получить список клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<List<CustomerShortResponse>>> GetCustomersAsync()
        {
            var list = await _unitOfWork.GetRepository<Customer>().GetAllAsync();

            var ModelList = list.Select(x =>
                new CustomerShortResponse()
                {
                    Id = x.Id,
                    Email = x.Email,
                    FirstName = x.FirstName,
                    LastName = x.LastName
                }).ToList();

            return Ok(ModelList);
        }

        /// <summary>
        /// получение одного клиента
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        [ProducesResponseType(typeof(CustomerResponse), (int)HttpStatusCode.OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            try
            {
                var customer = await _unitOfWork.GetRepository<Customer>().GetByIdAsync(id, c => c.PromoCodes);
                if (customer == null) return NotFound(new { message = $"Клиент с id={id} не найден" });

                List<PromoCodeShortResponse> listPromo = customer.PromoCodes.Select(x =>
                    new PromoCodeShortResponse()
                    {
                        Id = x.Id,
                        Code = x.Code,
                        BeginDate = x.BeginDate.ToString("dd.MM.yyyy"),
                        EndDate = x.EndDate.ToString("dd.MM.yyyy"),
                        PartnerName = x.PartnerName,
                        ServiceInfo = x.ServiceInfo
                    }).ToList();
                var preferences = await _unitOfWork.GetRepository<CustomerPreference>().GetByFilterAsync(c => c.CustomerId == id, p => p.Preference);
                List<PreferenceModel> listPreference = preferences.Select(x =>
                new PreferenceModel()
                {
                    Id = x.Id,
                    Name = x.Preference.Name
                }).ToList();
                var ModelList = new CustomerResponse
                {
                    Id = customer.Id,
                    Email = customer.Email,
                    FirstName = customer.FirstName,
                    LastName = customer.LastName,
                    PromoCodes = listPromo,
                    Preferences = listPreference
                };
                return Ok(ModelList);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Клиент не определен");
                return BadRequest(new { result = "error", errorMessage = "Клиент не определен" + ex.Message });
            }
        }

        /// <summary>
        /// Добавить клиента с его предпочтениями
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            try
            {
                var preferenceIds = request.PreferenceIds?.ToArray() ?? Array.Empty<Guid>();
                using var transaction = await _unitOfWork.BeginTransactionAsync();
                try
                {
                    var customerId = Guid.NewGuid();
                    await _unitOfWork.GetRepository<Customer>().AddAsync(new Customer
                    {
                        Id = customerId,
                        FirstName = request.FirstName,
                        LastName = request.LastName,
                        Email = request.Email
                    });
                    if (preferenceIds.Any())
                    {
                        await _unitOfWork.GetRepository<CustomerPreference>().AddRangeAsync(preferenceIds
                            .Select(preferenceId => new CustomerPreference
                            {
                                Id = Guid.NewGuid(),
                                CustomerId = customerId,
                                PreferenceId = preferenceId
                            }));
                    }

                    await transaction.CommitAsync();
                    return Ok(new { result = "ok", Message = "Добавление успешно!" });
                }
                catch
                {
                    await transaction.RollbackAsync();
                    throw;
                }
            }
            catch (Exception ex)
            {
               
                _logger.LogError(ex, "Ошибка добавления клиента");
                return BadRequest(new { result = ex.HResult, errorMessage = "Ошибка добавления клиента.  " + ex.InnerException.Message ?? ex.Message });
            }
        }

        /// <summary>
        /// Обновить данные клиента вместе с его предпочтениями
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _unitOfWork.GetRepository<Customer>().GetByIdAsync(id);
            if (customer == null) return NotFound(new { message = $"Клиент с id={id} не найден" });
            try
            {
                var preferenceIds = request.PreferenceIds?.ToArray() ?? Array.Empty<Guid>();
                using var transaction = await _unitOfWork.BeginTransactionAsync();
                try
                {
                    customer.Email = request.Email;
                    customer.FirstName = request.FirstName;
                    customer.LastName = request.LastName;

                    await _unitOfWork.GetRepository<Customer>().UpdateAsync(customer);
                    await _unitOfWork.GetRepository<CustomerPreference>().RemoveAsync(c => c.CustomerId == id);

                    if (preferenceIds.Any())
                    {
                        await _unitOfWork.GetRepository<CustomerPreference>().AddRangeAsync(preferenceIds
                            .Select(preferenceId => new CustomerPreference
                            {
                                Id = Guid.NewGuid(),
                                CustomerId = id,
                                PreferenceId = preferenceId
                            }));
                    }

                    await transaction.CommitAsync();
                    return Ok(new { result = "ok", Message = "Корректировка успешна!" });
                }
                catch
                {
                    await transaction.RollbackAsync();
                    throw;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Ошибка корректировки клиента");
                return BadRequest(new { result = "error", errorMessage = "Ошибка корректировки клиента" + ex.InnerException.Message ?? ex.Message });
            }
        }

        /// <summary>
        /// Удалить клиента вместе с выданными ему промо-кодами
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        [ProducesResponseType(typeof(Guid), (int)HttpStatusCode.OK)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            if (!await _unitOfWork.GetRepository<Customer>().AnyAsync(id))
            {
                return NotFound(new { message = $"Клиент с id={id} не найден" });
            }
            try
            {
                using var transaction = await _unitOfWork.BeginTransactionAsync();
                try
                {
                    await _unitOfWork.GetRepository<CustomerPreference>().RemoveAsync(c => c.CustomerId == id);
                    await _unitOfWork.GetRepository<PromoCode>().RemoveAsync(c => c.CustomerId == id);
                    await _unitOfWork.GetRepository<Customer>().RemoveAsync(id);

                    await transaction.CommitAsync();
                    return Ok(new { result = "ok", Message = "Удаление успешно!" });
                }
                catch
                {
                    await transaction.RollbackAsync();
                    throw;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Ошибка удаления клиента");
                return BadRequest(new { result = "error", errorMessage = "Ошибка удаления клиента" + ex.InnerException.Message ?? ex.Message });
            }
        }
    }
}