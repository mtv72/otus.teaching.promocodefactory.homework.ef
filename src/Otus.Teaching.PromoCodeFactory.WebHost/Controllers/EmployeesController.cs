﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Microsoft.AspNetCore.Http;
using System.Net;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController : BaseAPIController
    {
        private readonly IUnitOfWork _unitOfWork;

        public EmployeesController(IUnitOfWork uow, ILogger<RolesController> logger, DBAppContext context) : base(logger, context)
        {
            _unitOfWork = uow;
        }

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<EmployeeShortResponse>>> GetEmployeesAsync()
        {
            var employees = await _unitOfWork.GetRepository<Employee>().GetAllAsync();

            var employeesModelList = employees.Select(x =>
                new EmployeeShortResponse()
                {
                    Id = x.Id,
                    Email = x.Email,
                    FullName = x.FirstName,                    
                }).ToList();

            return Ok(employeesModelList);
        }

        /// <summary>
        /// Получить данные сотрудника по id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _unitOfWork.GetRepository<Employee>().GetByIdAsync(id, e => e.Role);

            if (employee == null)
                return NotFound(new { message = $"Сотруднк с id={id} не найден" });

            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                FirstName = employee.FirstName,
                LastName = employee.LastName,
                FullName = employee.FullName,
                Role = new RoleItemResponse()
                {
                    Name = employee.Role.Name,
                    Description = employee.Role.Description
                },
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return Ok(employeeModel);
        }

        /// <summary>
        /// Добавить сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult> CreateEmployeeAsync(CreateOrEditEmployeeRequest employee)
        {
            try
            {

                var employeeId = Guid.NewGuid();
                await _unitOfWork.GetRepository<Employee>().AddAsync(new Employee()
                {
                    Id = employeeId,
                    Email = employee.Email,
                    FirstName = employee.FirstName,
                    LastName = employee.LastName,
                    AppliedPromocodesCount = employee.AppliedPromocodesCount,
                    RoleId = employee.RoleId
                });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Ошибка добавления сотрудника!");
                return BadRequest(new { result = "error", errorMessage = "Ошибка добавления клиента.  " + ex.InnerException.Message ?? ex.Message });
            }

            return Ok(new { result = "ok", Message = "Добавление успешно!" });
        }
        /// <summary>
        /// Изменить сотрудника
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <param name="employee">объект для вставки</param>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult> EditEmployeeAsync(Guid id, CreateOrEditEmployeeRequest employee)
        {
            var existEmployee = await _unitOfWork.GetRepository<Employee>().GetByIdAsync(id);

            if (existEmployee == null)
            {
                return NotFound(new { message = $"Сотрудник с id={id} не найден" });
            }

            try
            {
                using var transaction = await _unitOfWork.BeginTransactionAsync();
                try
                {

                    existEmployee.RoleId = employee.RoleId;
                    existEmployee.Email = employee.Email;
                    existEmployee.LastName = employee.LastName;
                    existEmployee.FirstName = employee.FirstName;
                    existEmployee.AppliedPromocodesCount = employee.AppliedPromocodesCount;

                    await _unitOfWork.GetRepository<Employee>().UpdateAsync(existEmployee);

                    await transaction.CommitAsync();
                    return Ok(new { result = "ok", Message = "Корректировка успешна!" });
                }
                catch
                {
                    await transaction.RollbackAsync();
                    throw;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Ошибка корректировки клиента");
                return BadRequest(new { result = "error", errorMessage = "Ошибка корректировки клиента" + ex.InnerException.Message ?? ex.Message });
            }
        }
        /// <summary>
        /// Удалить сотрудника
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        [ProducesResponseType(typeof(Guid), (int)HttpStatusCode.OK)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult> DeleteEmployeeAsync(Guid id)
        {
            if (!await _unitOfWork.GetRepository<Employee>().AnyAsync(id))
            {
                return NotFound(new { message = $"Сотрудник с id={id} не найден" });
            }

            try
            {
                await _unitOfWork.GetRepository<Employee>().RemoveAsync(id);
                return Ok(new { result = "ok", Message = "Удаление успешно!" });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Ошибка удаления сотрудника");
                return BadRequest(new { result = "error", errorMessage = "Ошибка удаления сотрудника" + ex.InnerException.Message ?? ex.Message });
            }

        }

    }
}