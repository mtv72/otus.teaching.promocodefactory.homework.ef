﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class PreferencesController : BaseAPIController
    {
        private readonly IUnitOfWork _unitOfWork;
        public PreferencesController(IUnitOfWork uow, ILogger<PreferencesController> logger, DBAppContext context) : base(logger, context)
        {
            _unitOfWork = uow;
        }

        /// <summary>
        /// Получить все доступные предпочтения
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<PreferenceModel>> GetPreferencesAsync()
        {
            var preferences = await _unitOfWork.GetRepository<Preference>().GetAllAsync();

            var preferencesModelList = preferences.Select(x =>
                new PreferenceModel()
                {
                    Id = x.Id,
                    Name = x.Name
                }).ToList();

            return Ok(preferencesModelList);
        }
        /// <summary>
        /// Получить предпочтение по идентификатору
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        [ProducesResponseType(typeof(PreferenceModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<PreferenceModel>> GetPreferenceByIdAsync(Guid id)
        {
            var preference = await _unitOfWork.GetRepository<Preference>().GetByIdAsync(id);
            if (preference == null)
                return NotFound(new { message = $"Предпочтение с id={id} не найдено" });

            var PreferenceModel = new PreferenceModel()
            {
                Id = preference.Id,
                Name = preference.Name
            };
            return Ok(PreferenceModel);
        }
        /// <summary>
        /// Добавить предпочтение
        /// </summary>
        /// <returns></returns>
        [HttpPost("add")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult> AddAsync([FromBody] PreferenceModel preference)
        {
            if (await _unitOfWork.GetRepository<Preference>().AnyAsync(preference.Id))
            {
                return Conflict(new { message = "Такая предпочтение уже существует!" });
            }

            try
            {
                await _unitOfWork.GetRepository<Preference>().AddAsync(new Preference
                {
                    Id = preference.Id,
                    Name = preference.Name
                });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Ошибка выполнения метода AddAsync!");
                return BadRequest(new { result = "error", errorMessage = "Ошибка выполнения метода AddAsync" + ex.Message });
            }

            return Ok(new { result = "ok", errorMessage = "Добавление успешно!" });
        }

        /// <summary>
        /// Изменить предпочтение
        /// </summary>
        /// <returns></returns>
        [HttpPut("update")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult> UpdateAsync([FromBody] PreferenceModel preference)
        {
            var updPreference = await _unitOfWork.GetRepository<Preference>().GetByIdAsync(preference.Id);
            if (updPreference == null)
                return NotFound(new { message = $"Предпочтение с id={preference.Id} не найдено" });

            try
            {
                updPreference.Name = preference.Name;

                await _unitOfWork.GetRepository<Preference>().UpdateAsync(updPreference);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Ошибка выполнения метода UpdateAsync!");
                return BadRequest(new { result = "error", errorMessage = "Ошибка выполнения метода UpdateAsync" + ex.Message });
            }

            return Ok(new { result = "ok", errorMessage = "Изменено успешно!" });
        }
        /// <summary>
        /// Удалить предпочтение
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult> DeleteAsync(Guid Id)
        {
            if (!await _unitOfWork.GetRepository<Preference>().AnyAsync(Id))
            {
                return NotFound(new { message = $"Предпочтение с id={Id} не найдено" });
            }

            try
            {
                await _unitOfWork.GetRepository<Preference>().RemoveAsync(Id);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Ошибка выполнения метода DeleteAsync!");
                return BadRequest(new { result = "error", errorMessage = "Ошибка выполнения метода DeleteAsync" + ex.Message });
            }

            return Ok(new { result = "ok", errorMessage = "Удалено успешно!" });
        }

    }
}
