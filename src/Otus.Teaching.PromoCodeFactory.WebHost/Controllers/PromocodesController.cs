﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController : BaseAPIController
    {
        private readonly IUnitOfWork _unitOfWork;
        public PromocodesController(IUnitOfWork uow, ILogger<RolesController> logger, DBAppContext context) : base(logger, context)
        {
            _unitOfWork = uow;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var list = await _unitOfWork.GetRepository<PromoCode>().GetAllAsync();

            var promoCode = list.Select(x => new PromoCodeShortResponse
            {
                Id = x.Id,
                BeginDate = x.BeginDate.ToString("dd.MM.yyyy HH:mm:ss zzz"),
                EndDate = x.EndDate.ToString("dd.MM.yyyy HH:mm:ss zzz"),
                Code = x.Code,
                PartnerName = x.PartnerName,
                ServiceInfo = x.ServiceInfo
            }).ToList();
            return Ok(promoCode);
        }

        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var preference = (await _unitOfWork.GetRepository<Preference>().GetByFilterAsync(x => x.Name.Equals(request.Preference)))?.FirstOrDefault();
            if (preference == null)
            {
                return BadRequest(new { result = "error", errorMessage = "Предпочтение не определено" });
            }
            try
            {
                DateTimeOffset dateNew = DateTimeOffset.UtcNow;
                var promoCodes = request.PromoCodes.Select(promoCode => new PromoCode
                {
                    Id = Guid.NewGuid(),
                    BeginDate = dateNew,
                    EndDate = DateTimeOffset.UtcNow.AddDays(30),
                    Code = promoCode,
                    PartnerName = request.PartnerName,
                    ServiceInfo = request.ServiceInfo,
                    PreferenceId = preference.Id,
                    PartnerManagerId = request.PartnerManagerId,
                    
                });

                //полный список клиентов с предпочтением
                var customers = await _unitOfWork.GetRepository<Customer>()
                                                 .GetByFilterAsync(x => x.CustomerPreferences.Any(p => p.PreferenceId == preference.Id)
                                                                 , c => c.PromoCodes);
                //те у кого нет ни одного промокода по задоному предпочтению
                var customer1=  customers.Where(x => !x.PromoCodes.Any(e => e.PreferenceId == preference.Id));
                //customers - customer1
                var customer2 = customers.Where(x => !customer1.Contains(x));
                //список тех у кого был промокод но закончился
                customer2.GroupBy(e => e.PromoCodes.DefaultIfEmpty().Where(l => l.EndDate < dateNew)).OrderBy(x => x.Key).SelectMany(x => x.ToList());

                var queue = new Queue<Customer>();
                //собрать в кучу customer2+customers1
                var rezCustomer = customer1.Union(customer2).ToList();
                rezCustomer.ForEach(queue.Enqueue);


                using var transaction = await _unitOfWork.BeginTransactionAsync();
                try
                {
                    foreach (var promoCode in promoCodes)
                    {
                        var customerQueue = queue.Peek();

                        promoCode.CustomerId = customerQueue.Id;

                        await _unitOfWork.GetRepository<PromoCode>().AddAsync(promoCode);

                        if (queue.Count > 1)
                        {
                            queue.Dequeue();
                        }
                    }
                    await transaction.CommitAsync();
                    }
                catch
                {
                    await transaction.RollbackAsync();
                    throw;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Ошибка GivePromoCodesToCustomersWithPreferenceAsync");
                return BadRequest(new { result = "error", errorMessage = ex.InnerException.Message ?? ex.Message });
            }

            return Ok(new { result = "ok", Message = "Успех!" });
        }
        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet("getAll")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromoCodesAllAsync()
        {
            var list = await _unitOfWork.GetRepository<PromoCode>().GetAllAsync(p => p.PartnerManager, p => p.Customer, p => p.Preference);

            return Ok(list.Select(promoCode => new PromoCodeModel()
            {
                Id = promoCode.Id,
                BeginDate = promoCode.BeginDate.ToString("dd.MM.yyyy HH:mm:ss zzz"),
                EndDate = promoCode.EndDate.ToString("dd.MM.yyyy HH:mm:ss zzz"),
                Code = promoCode.Code,
                PartnerName = promoCode.PartnerName,
                ServiceInfo = promoCode.ServiceInfo,
                Preference = promoCode.Preference == null ? null
                : new PreferenceModel
                {
                    Id = promoCode.PreferenceId,
                    Name = promoCode.Preference.Name
                },
                Customer = promoCode.Customer == null ? null: new CustomerShortResponse
                {
                    Id = promoCode.Customer.Id,
                    Email = promoCode.Customer.Email,
                    FirstName = promoCode.Customer.FirstName,
                    LastName=promoCode.Customer.LastName
                },
                PartnerManager = promoCode.PartnerManager == null ? null: new EmployeeShortResponse
                {
                    Id = promoCode.PartnerManager.Id,
                    Email = promoCode.PartnerManager.Email,
                    FullName = promoCode.PartnerManager.FullName
                }
            }).ToList());
        }

        /// <summary>
        /// Получить промокод по идентификатору
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<PromoCodeModel>> GetPromoCodeByIdAsync(Guid id)
        {
            var promoCode = await _unitOfWork.GetRepository<PromoCode>().GetByIdAsync(id, p => p.PartnerManager, p => p.Customer, p => p.Preference);
            if (promoCode == null) return NotFound();

            return new PromoCodeModel()
            {
                Id = promoCode.Id,
                BeginDate = promoCode.BeginDate.ToString("dd.MM.yyyy HH:mm:ss zzz"),
                EndDate = promoCode.EndDate.ToString("dd.MM.yyyy HH:mm:ss zzz"),
                Code = promoCode.Code,
                PartnerName = promoCode.PartnerName,
                ServiceInfo = promoCode.ServiceInfo,
                Preference = promoCode.Preference == null ? null
                : new PreferenceModel
                {
                    Id = promoCode.PreferenceId,
                    Name = promoCode.Preference.Name
                },
                Customer = promoCode.Customer == null ? null: new CustomerShortResponse
                {
                    Id = promoCode.Customer.Id,
                    Email = promoCode.Customer.Email,
                    FirstName=promoCode.Customer.FirstName,
                    LastName = promoCode.Customer.LastName
                },
                PartnerManager = promoCode.PartnerManager == null ? null
                : new EmployeeShortResponse
                {
                    Id = promoCode.PartnerManager.Id,
                    Email = promoCode.PartnerManager.Email,
                    FullName = promoCode.PartnerManager.FullName
                }
            };
        }
    }
}