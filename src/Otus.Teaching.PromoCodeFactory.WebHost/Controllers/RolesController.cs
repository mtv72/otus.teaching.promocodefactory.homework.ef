﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System.Net;
using Microsoft.AspNetCore.Http;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Роли сотрудников
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class RolesController: BaseAPIController
    {
        private readonly IUnitOfWork _unitOfWork;

        public RolesController(IUnitOfWork uow, ILogger<RolesController> logger, DBAppContext context) : base( logger, context)
        {
            _unitOfWork = uow;
        }

        /// <summary>
        /// Получить все доступные роли сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IEnumerable<RoleItemResponse>> GetRolesAsync()
        {
            var roles = await _unitOfWork.GetRepository<Role>().GetAllAsync();

            var rolesModelList = roles.Select(x => 
                new RoleItemResponse()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description
                }).ToList();

            return rolesModelList;
        }
        /// <summary>
        /// Получить роль по идентификатору
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<RoleItemResponse>> GetRoleByIdAsync(Guid id)
        {
            var role_ = await _unitOfWork.GetRepository<Role>().GetByIdAsync(id);
            if (role_ == null) return NotFound(new { message = $"Роль с id={id} не найдена!" });

            return new RoleItemResponse()
            {
                Id = role_.Id,
                Name = role_.Name,
                Description = role_.Description
            };
        }

        /// <summary>
        /// Добавить роль
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> CreateAsync([FromBody] RoleItemResponse role)
        {
            if (await _unitOfWork.GetRepository<Role>().AnyAsync(role.Id))
            {
                return Conflict(new { message = "Такая роль уже существует!" });
            }

            try
            {
                await _unitOfWork.GetRepository<Role>().AddAsync(new Role
                {
                    Id = role.Id,
                    Name = role.Name,
                    Description = role.Description
                });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Ошибка добавления роли!");
                return BadRequest(new { result = "error", errorMessage = "Ошибка добавления роли" + ex.Message });
            }

            return Ok(new { result = "ok", errorMessage = "Добавление успешно!" });
        }
        /// <summary>
        /// Изменить роль
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> UpdateAsync([FromBody] RoleItemResponse role)
        {
            var role_ = await _unitOfWork.GetRepository<Role>().GetByIdAsync(role.Id);
            if (role_ == null) return NotFound(new { message = $"Роль с id={role.Id} не найдена!" });

            try
            {
                role_.Description = role.Description;
                role_.Name = role.Name;

                await _unitOfWork.GetRepository<Role>().UpdateAsync(role_);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Ошибка обновлении роли!");
                return BadRequest(new { result = "error", errorMessage = ex.Message });
            }

            return Ok(new { result = "ok", errorMessage = $"Обновление успешно!" });
        }

        /// <summary>
        /// Удалить роль
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> DeleteAsync(Guid id)
        {
            if (!await _unitOfWork.GetRepository<Role>().AnyAsync(id))
            {
                return NotFound(new { message = $"Роль с id={id} не найдена!" });
            }

            try
            {
                await _unitOfWork.GetRepository<Role>().RemoveAsync(id);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Ошибка при удалении роли!");
                return BadRequest(new { result = "error", errorMessage = ex.Message });
            }

            return Ok(new { result = "ok", errorMessage = $"Удалено!" });
        }

    }
}