using Microsoft.Extensions.DependencyInjection;
using System;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost
{
    public static class DataAccessModule
    {
        public sealed class ModuleConfiguration
        {
            public IServiceCollection Services { get; init; }
        }
        public static IServiceCollection AddDataAccessModule(this IServiceCollection services, Action<ModuleConfiguration> action)
        {
            var moduleConfiguration = new ModuleConfiguration
            {
                Services = services
            };
            action(moduleConfiguration);
            return services;
        }
        public static void IServiceCollection(this ModuleConfiguration moduleConfiguration, string connectionString)
        {
            moduleConfiguration.Services.AddDbContext<DBAppContext>(options =>
            {          
                options.UseSqlite(connectionString);
            });

            moduleConfiguration.Services.AddScoped<IRepository<Employee>, EfRepository<Employee>>();
            moduleConfiguration.Services.AddScoped<IRepository<Role>, EfRepository<Role>>();
            moduleConfiguration.Services.AddScoped<IRepository<Customer>, EfRepository<Customer>>();
            moduleConfiguration.Services.AddScoped<IRepository<CustomerPreference>, EfRepository<CustomerPreference>>();
            moduleConfiguration.Services.AddScoped<IRepository<Preference>, EfRepository<Preference>>();
            moduleConfiguration.Services.AddScoped<IRepository<PromoCode>, EfRepository<PromoCode>>();

            moduleConfiguration.Services.AddScoped<IUnitOfWork, UnitOfWork>();

        }
    }
}
