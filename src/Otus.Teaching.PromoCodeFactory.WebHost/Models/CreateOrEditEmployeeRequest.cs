﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class CreateOrEditEmployeeRequest
    {
        [MaxLength(200), Required(AllowEmptyStrings = false)]
        public string FirstName { get; set; }
        [MaxLength(200)]
        public string LastName { get; set; }
        [MaxLength(300)]
        public string Email { get; set; }
        public Guid RoleId { get; set; }
        public int AppliedPromocodesCount { get; set; }
    }
}
