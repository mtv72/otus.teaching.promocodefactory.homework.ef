﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class PromoCodeModel : PromoCodeShortResponse
    {
        public EmployeeShortResponse PartnerManager { get; set; }
        public PreferenceModel Preference { get; set; }
        public CustomerShortResponse Customer { get; set; }
    }
}
